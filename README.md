# pourquoi-python.fr

L'objectif est d'intéresser des personnes non-techniques, non-dev, en donnant un aperçu 
des domaines qu'on peut attaquer avec Python. C'est aussi une occasion de faire de la 
veille et d'enrichir le site régulièrement.

Le site est accessible sur [https://www.pourquoi-python.fr](https://www.pourquoi-python.fr)

## Participer

### Initialisation de l'environnement de dev

```sh
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

Puis `mkdocs serve` pour accéder à la documentation autogénérée 
[en local](http://127.0.0.1:8000) (port 8000).

### Proposer des changements

* Créer une branche dédiée
* pousser les modifications
* créer une merge request
* vérifier que la pipeline passe (`mkdocs build` en mode `--strict`)
* échanger avec les membres du projet jusqu'à validation de la MR
* fusionner sur la branche `main` = mise à jour du site
