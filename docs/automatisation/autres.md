---
tags:
    - Automatisable
---

# Autres logiciels pilotables

De très nombreux autres logiciels sont pilotables nativement en Python. Pour
les autres, sous Windows, sous Mac et sous Linux, des API ou des bus
d'événements de type COM, D-Bus, AppleScript, sont également
programmables en Python pour piloter des applications, des parties du
système ou réagir à des événements.
