---
tags:
    - Graphisme
    - Automatisable
---

# Blender

![Blender](/img/blender.png)

Blender est un logiciel de modelage et d'animation 3D libre et très puissant,
entièrement scriptable en Python, ce qui permet d'automatiser des traitements,
des créations d'objets ou d'animations complexes.

![Blender](/img/blender-exemple.png)
