---
tags:
    - Graphisme
    - Automatisable
---

# Gimp

Gimp est un logiciel libre de retouche d'images possédant
de nombreuses fonctions avancées. Il inclut de base un
interprète Python, permettant de le scripter et de réaliser
des filtres très puissants.

![GIMP](/img/gimp.png)
