---
tags:
    - Graphisme
    - Automatisable
---

# Inkscape

Inkscape est un illustrateur vectoriel libre, il est aussi
interfaçable avec Python, de l'intérieur ou de l'extérieur
(appel d'un script depuis Inkscape ou communication avec
Inkscape depuis un script).

![Inkscape](/img/inkscape.png)
