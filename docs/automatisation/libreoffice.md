---
tags:
    - Graphisme
    - Automatisable
---

# LibreOffice

La célèbre suite bureautique libre peut être pilotée entièrement en Python. Il
est donc possible d'écrire des macros en Python pour réaliser toutes sortes de
tâches automatisée, de générer ou remplir des documents depuis une base de
données, générer des factures, etc.

![LibreOffice](/img/libreoffice.png)
