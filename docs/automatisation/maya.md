---
tags:
    - Graphisme
    - Automatisable
---

# Autodesk Maya

Maya est un modeleur 3d propriétaire réputé, développé par Autodesk. Depuis
la version 8.5, Maya offre nativement une interface Python aux commandes
Maya et à l'API interne.
