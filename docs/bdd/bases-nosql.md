---
tags:
    - NoSQL
    - Bases de données
---

# Bases de données « NoSQL »

Il s'agit souvent de bases de données à très grands
volumes de stockage, souvent distribuées. Un bon
exemple de base de ce type est CouchDB, une base de
données supportée par la fondation Apache, écrite en
Erlang avec un système de vues en Javascript. Le
principe de CouchDB est de stocker uniquement des
documents non structurés, possédant chacun un
ensemble non figé de propriétés.

Les documents sont stockés à plat, de manière non
hiérarchique. L'équivalent des requêtes SQL se fait
grâce à l'utilisation de « vues » pré-calculées pour
l'ensemble des documents. Le dialogue avec CouchDB
peut se faire avec n'importe quel langage, via HTTP et
un échange de données en JSON. Plusieurs
bibliothèques en Python, comme CouchDBKit, facilitent
le dialogue, la création de bases, l'insertion ou la
modification des vues.

MongoDB est un autre exemple de base orientée « document », et possédant
un pilote Python.

La tendance NoSQL suit divers chemins : en dehors des bases orientées
document comme CouchDB et MongoDB, on trouve des bases orientées
« clé/valeur » comme Redis ou MemcacheDb, ou orientées « colonnes »,
comme Cassandra qui a été libérée par Facebook en 2008. Toutes les bases
de ce type possèdent un pilote ou une bibliothèque Python : celle de
Cassandra se nomme Lazyboy et simplifie l'accès aux données en s'intégrant
aux structures de données haut niveau de Python. Celle de Redis se nomme
simplement « redis », etc.

![Bases NoSQL](/img/nosql.png)
