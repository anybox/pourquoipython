---
tags:
    - Écrit en Python
    - Base de données
---

# Bases de données objet

Des bases de données objets sont également en production depuis plus de
dix ans et simplifient la persistance des données dans le cadre d'un langage
orienté objet comme Python. L'une d'entre elles, la ZODB, provient du
framework web Zope, mais peut être utilisée de manière complètement
indépendante pour permettre la persistance d'objets Python dans n'importe
quelle application. L'utilisation de la ZODB rend la persistance complètement
transparente dans une application : les objets sont stockés tels quels sous
forme d'un graphe hiérarchique et chacun peut être utilisé directement
comme un objet déjà en mémoire, sans avoir à effectuer la moindre requête.
Une ZODB est capable de gérer des millions d'objets de manière fiable,
transactionnelle, et historisée. Elle répond aux quatre propriétés ACID :
Atomicité, Cohérence, Isolation et Durabilité.
