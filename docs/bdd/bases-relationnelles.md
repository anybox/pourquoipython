---
tags:
    - Base de données
---

# Bases de données relationnelles

Il s'agit des bases de données classiques que l'on
peut interroger avec le langage SQL. La majorité de
ces bases de données, aussi bien libres que
propriétaires, possèdent un pilote Python et ces
pilotes suivent une spécification commune : la DB-
API. La façon d'ouvrir un accès à une base et de
lancer une requête est donc la même quelle que soit
la base de données. Nul besoin d'apprendre à
utiliser chaque pilote indépendamment. Ceci facilite
également la migration d'une base vers une autre.

Parmi les bases de données ou les API prises en
charge on trouve (liste non exhaustive) :

 - PostgreSQL
 - MySQL
 - Oracle
 - SQLite
 - Ingres
 - Informix
 - Sybase
 - Microsoft ADO
 - IBM DB2
 - ODBC
 - Berkeley DB

![Bases de données relationnelles](/img/bdd.png)
