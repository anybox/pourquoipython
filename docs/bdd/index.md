---
tags:
    - Base de données
---

# Accès aux bases de données

La plateforme Python permet d'accéder de manière très simple à la majorité des bases de données actuelles, y compris à des bases de données émergentes suivant la tendance actuelle du « NoSQL ». En dehors des pilotes bas niveau, on trouve également un ensemble de surcouches facilitant la programmation et améliorant fortement la sécurité (par exemple des protections contre les injections SQL).
