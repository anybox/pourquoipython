---
tags:
    - Base de données
---

# Surcouches pour l'accès aux bases

Python étant un langage objet, il est souvent plus agréable d'accéder aux bases
de données en utilisant des objets. Au delà de la spécification DB-API offrant
une interface unifiée, on trouve différents niveaux de surcouches aux bases de
données : des bibliothèques comme SQLAlchemy proposent la construction de
requêtes SQL par de simples appels de méthode de type insert, filter ou join
sur des objets, sans avoir à écrire une seule ligne de SQL. Cela présente de
nombreux avantages en terme de fiabilité, de sécurité et de possibilité de
changement de base de données : le langage d'accès est unifié entre toutes les
bases.

![ORM](/img/orm.png)

À un niveau encore supérieur, on trouve les « ORM » (Object Relational
Mappers). SQLAlchemy contient aussi un excellent ORM, probablement le plus
utilisé en Python. L'ORM permet de travailler directement sur les objets
métiers sans se soucier de la base de données : lors d'une lecture dans la
base, les objets Python sont automatiquement reconstruits par agrégation ou
jointure de tables. Inversement, lorsqu'on modifie les attributs d'un objet,
l'ORM sait dans quelles tables écrire. Cette connaissance de l'ORM provient de
l'écriture des schémas de données métiers effectuée lors de la phase de
conception : la gestion de la base de données n'a plus besoin d'être prise en
charge par le développeur, hormis pour des questions d'optimisation.
Heureusement pour ce dernier point, il est possible de connaître toutes les
requêtes générées et d'agir même au plus bas niveau.
