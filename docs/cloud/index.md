---
tags:
    - Cloud
---

# Cloud

De par sa nature dynamique et multi-plateforme, et aidé
par sa syntaxe simple et claire, Python se révèle être un
très bon langage pour des besoins d'infrastructure allant
d'un petit script système à une infrastructure Cloud
géante. Ainsi on le retrouve de façon de plus en plus
fréquente dans de nombreux outils d'installation, de
configuration, de maintenance, de surveillance, de
déploiement ou d'hébergement.
La gestion des exceptions, avec les erreurs
possibles clairement identifiées dans les
modules systèmes, permet ainsi de gérer
finement et clairement les différents états
dans lesquels une opération peut se terminer. L'intégration à
l'environnement, la gestion simple des variables systèmes, des
arborescences, des journaux d'exécutions, des expressions
rationnelles puissantes permettent entre autres d'écrire des
scripts systèmes efficaces, lisibles et maintenables.

![Ubuntu](/img/ubuntu.png)

![Fedora](/img/fedora.png)

![Redhat](/img/redhat.png)

![Debian](/img/debian.png)

