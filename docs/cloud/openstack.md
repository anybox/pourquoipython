---
tags:
    - Cloud
    - Écrit en Python
---

# OpenStack

Openstack est un monumental ensemble de logiciels
consacrés à la création de clouds privés et publics. Il est
devenu en quelques années la référence open-source
internationale en matière de Cloud à tel point que des
entreprises comme HP ont investi en 2014 un milliard de
dollars dans cette solution, et qu'il est utilisé par
exemple en France comme infrastructure pour offrir des
services de Cloud souverain. OpenStack est écrit
majoritairement en Python.

![Openstack](/img/openstack.png)

