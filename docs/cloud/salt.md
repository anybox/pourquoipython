---
tags:
    - Configuration
    - Provisionnement
    - Automatisation
    - Écrit en Python
---

# Salt

![Salt](/img/salt.png)

Salt facilite l'automatisation, le provisionning, l'orchestration et la gestion
de configuration de parcs d'applications, de machines physiques, de serveurs
virtuels ou de Clouds en tous genres.
