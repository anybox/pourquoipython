---
tags:
    - Écrit en Python
    - DevOps
    - Intégration continue
---

# Buildbot

![Buildbot](/img/buildbot.png)

Pour améliorer la qualité et la réactivité d'un développement, on peut mettre
en place un robot qui lance les tests unitaires et fonctionnels
automatiquement, de manière périodique ou de manière synchrone dès qu'une
modification est effectuée sur le code. L'équipe peut être ainsi avertie
immédiatement et sans intervention humaine.  L'un des outils les plus utilisés
est BuildBot, un robot d'intégration continue écrit en Python. Cet outil est
très souple. Un ensemble de clients BuildBots peut être lancé sur différentes
machines, par exemple un Windows 32bits, un Linux 64bits, et renvoyer les
informations à un serveur Buildbot qui affiche un tableau récapitulatif de tous
les tests et de leurs résultats. En même temps que les tests unitaires, le
BuildBot peut exécuter et publier les analyses de qualité de code. En fonction
de ce résultat, n'importe quelle action automatique peut être entreprise, comme
l'envoi d'un e-mail ou le hurlement d'un Tuxdroïd.  Les métriques de qualité
peuvent aussi être recueillies par des outils comme Bitten pour faciliter
l'intégration continue. L'intérêt de Bitten est qu'il s'intègre dans la forge
Trac, pour rapprocher les métriques qualité de la gestion des tickets, la
documentation ou le code source.

![Buildbot](/img/buildbot-exemple.png)
