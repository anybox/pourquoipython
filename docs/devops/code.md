---
tags:
    - Qualité
---

# Qualité du code

## Analyse automatique

Comme déjà mentionné dans le premier chapitre, un code
Python doit préférablement suivre un ensemble de conventions de codage
(définies dans le document PEP-8). En plus de ces conventions, il est possible
de définir un ensemble de règles spécifiques à un projet, ou de contraintes
supplémentaires sur le code. Des outils d'analyse du code sont dans ce cas
utiles pour vérifier ces règles, et définir un ensemble de métriques de qualité
à respecter. Ces métriques peuvent alors être vérifiées en même temps que les
tests unitaires.  Parmi les outils d'analyse automatique du code, on peut citer
Pylint, PyFlakes Flake8 ou PyChecker. Ces outils sont capables de détecter des
problèmes potentiels dans le code, un manque de documentation, un non-respect
des conventions, des imports inutiles, des morceaux de code jamais exécutés,
etc.  Des indicateurs supplémentaires existent pour détecter par exemple des
problèmes de conception, mesurer la complexité cyclomatique du code, ou trouver
des redondances potentielles.

## Analyse humaine

L'analyse humaine se pratique
notamment grâce à des outils de revue de code. L'application Review Board,
écrite elle-même en Python (Django), prend en charge cette fonctionnalité : au
travers d'une interface web conviviale, le code source peut être commenté,
toute modification peut être soumise à approbation avant ou après propagation
dans le dépôt.

![Revieboard](/img/reviewboard.png)

## Analyse aléatoire

Les outils de fuzzing peuvent détecter des
bugs ou des problèmes de sécurité en soumettant au programme des données
invalides générées aléatoirement, ou générées à partir de données valides dont
quelques bits ont été modifiés aléatoirement. Plusieurs outils de ce genre sont
écrits en Python, l'un d'entre eux (Fusil) est capable de tester n'importe quel
programme et a même servi à détecter des problèmes dans de nombreux logiciels
tels que Gimp, PHP ou Python lui-même.
