---
tags:
    - Intégration continue
    - DevOps
---

# Construction d'applications

Une application se compose généralement d'un assemblage de produits, de
paquets, de bases de données, de bibliothèques, de code source spécifique,
ou de configuration. Pour assembler tous ces éléments de manière
automatique et répétable, il est possible d'utiliser un outil de construction.
Buildout en fait partie et est très utilisé dans le milieu du web. Il se base sur
une succession de « recettes », chaque recette étant responsable d'une partie
de l'application, par exemple la mise en place de la base de données ou d'un
distributeur de charge. Les recettes sont elles-mêmes distribuées sous forme
de paquets Python et sont disponibles pour tous. Un unique fichier de
configuration de quelques dizaines de lignes est suffisant pour décrire et
construire une application complexe en puisant dans diverses sources.
