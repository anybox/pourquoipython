---
tags:
    - Déploiement
    - DevOps
---

# Déploiement d'applications

Le déploiement s'automatise facilement, soit en utilisant l'outil Buildout
mentionné dans le paragraphe précédent, éventuellement en générant une
archive ou un paquet système contenant l'application. Des outils additionnels
comme Fabric peuvent être utilisés pour automatiser des tâches
simultanément sur plusieurs serveurs. Pour des cas plus complexes, Salt peut
avantageusement être utilisé pour effectuer des déploiements et
configurations en parallèle.
