---
tags:
    - DevOps
---

# Dépôts de code source

Les dépôts de code source contiennent tout l'historique,
ligne par ligne, de l'évolution du code source d'un projet.
En dehors de Subversion, encore très utilisé, la tendance
actuelle est à l'utilisation de gestionnaires de version
décentralisés comme Mercurial, Bazaar ou Git. Sur ces trois
systèmes, les deux premiers sont écrits en Python ! Leur
intérêt est multiple, ils ne nécessitent pas de serveur
central, sont faciles à utiliser, autorisent un travail distant
hors-ligne, s'adaptent à n'importe quelle organisation
d'équipe, et offrent de par leur fonctionnement une
réplication intrinsèque pouvant servir de sauvegarde. De très nombreux
projets migrent actuellement vers ces systèmes, grâce à la facilité avec
laquelle on peut convertir un dépôt Subversion en dépôt décentralisé.

![Mercurial](/img/mercurial.png)
