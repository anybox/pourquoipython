---
tags:
    - Qualité
---

# Qualité

Par défaut et dans l'esprit, Python n'oblige pas les développeurs à utiliser des
outils lourds et contraignants, un simple éditeur de texte et un terminal sont
suffisants pour développer des applications de toutes tailles, y compris les
plus importantes. La courbe d'apprentissage est ainsi beaucoup plus douce
que dans d'autres environnements.
Néanmoins tous les outils sont présents pour s'adapter aux goûts de chacun,
aux pratiques modernes ainsi qu'aux contraintes industrielles. Cette section
présente des outils qui doivent être mis en place pour améliorer la qualité des
projets, l'intégration continue ou la réactivité des équipes.


