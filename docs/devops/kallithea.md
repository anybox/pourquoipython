---
tags:
    - Forge logicielle
---

# Kallithea

![Kallithea](/img/kallithea.png)

Kallithea est une autre forge open- source écrite en Python, issue d'un fork de
RhodeCode et qui a évolué de manière très positive pour arriver à un niveau
proche des solutions propriétaires en ligne comme Github ou Bitbucket. Elle
offre aujourd'hui tout ce dont une équipe de développement a besoin :
navigation dans des dépôts Git ou Mercurial, revue et commentaire de code,
notifications, Gists, indexation des dépôts, Pull requests, etc.
