# Catalogue des paquets Python

Les composants applicatifs en Python peuvent être distribués sous forme de
paquets individuels appelés des eggs. Un projet peut donc être décomposé en
plusieurs eggs et ceux-ci peuvent être facilement réutilisés dans d'autres
projets.  Dans le cadre d'une gestion de projet, le découpage en eggs facilite
la répartition des tâches entre plusieurs équipes qui peuvent publier
individuellement les versions de leur module pour les intégrer au produit
final. La gestion de projet elle-même peut donc être séparée et le module peut
suivre son propre cycle de développement, test, publication, intégration, mise
en production, et maintenance corrective.  Les composants ayant un intérêt
général sont publiés sur un site web regroupant l'ensemble de ce qui est
produit par la communauté : l'index PyPI (Python Package Index). Cette
pratique, courante dans le milieu du logiciel libre, améliore la mutualisation
du code, évite de réinventer ce qui existe déjà, permet de trouver facilement
un module pour un besoin particulier, et encourage tout le monde à écrire du
code générique réutilisable.  Grâce à des outils comme **PIP**, tout composant
présent sur l'index PyPI peut être téléchargé et installé d'une simple commande
ou ajouté à un projet par une simple ligne dans un fichier de configuration. À
l'inverse, un composant peut être publié sur l'index PyPI avec la même
facilité, ou éventuellement sur un index privé, interne à l'entreprise.

![PyPI](/img/pypi.png)
