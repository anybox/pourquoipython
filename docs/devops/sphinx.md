---
tags:
    - Documentation
    - Écrit en Python
---

# Sphinx

L'univers Python utilise une syntaxe commune pour la documentation, les
commentaires de code ou les doctests : le reStructuredText (RST). Le RST a la
particularité d'être conçu pour être lu directement. Il ne comporte pas
d'éléments gênant la lecture tels que les balises ou les délimiteurs de blocs
(crochets, accolades...), mais est inspiré des pratiques intuitives d'écriture de
contenu en mode texte pur. Voici un exemple :

```rst
Outils d'industrialisation
==========================

Par exemple, les titres de chapitre sont soulignés de signes 'egal'

Génération de documentation
---------------------------
Les titres de paragraphe sont soulignés de tirets,
l'emphase est indiquée avec des **étoiles** (gras) ou des `quotes`
(italique)
```

Python fournit tous les outils nécessaires pour
valoriser de la documentation écrite en RST. L'outil
phare de génération de documentation dans
l'écosystème Python est Sphinx. Cet outil génère
de la documentation au format HTML, PDF ou Latex
à partir de documents en RST. Le html produit offre
par défaut une interface web conviviale avec une
table des matières générée automatiquement et un
moteur de recherche intégré écrit en Javascript. Il
gère la coloration syntaxique des blocs de code pour de nombreux langages. Il
peut également inspecter un projet pour extraire la documentation du code et
la liste des composants.
Sphinx peut être utilisé pour n'importe quel projet de documentation. Le
phase initiale d'écriture de cette publication a elle-même été gérée avec cet
outil.
