---
tags:
    - DevOps
    - Qualité
---

# Tests automatisés

Si les tests unitaires peuvent se pratiquer dans tous les environnements, la
plateforme Python offre une notion complémentaire extrêmement bénéfique :
les « doctests ». On peut présenter les doctests au choix comme de la
documentation testée ou des tests documentés. Ils permettent de
documenter et tester un projet en même temps pendant la phase de
conception, avant même l'écriture du code. Il s'agit d'une succession de
paragraphes explicatifs et de morceaux de code de haut niveau donnant un
aperçu de l'utilisation des composants applicatifs dans une console Python.

En voici un exemple :

```python
Voici comment utiliser la classe `Ham` avec ses attributs
et ses méthodes. On peut créer une instance de la classe et modifier ses
attributs :

>>> from monprojet import Ham
>>> ham = Ham()
>>> ham.nom = 'nom du ham'
>>> ham.valeurs = [2, 3, 4]

Ensuite on peut calculer la moyenne des valeurs grâce à une méthode `mean` :

>>> ham.mean()
3.0
```

Cette documentation peut être exécutée comme un programme test et on
doit retrouver à l'exécution les mêmes valeurs de sortie que dans le texte.
Les doctests sont complémentaires aux tests unitaires ou fonctionnels et
garantissent que la documentation existe et est à jour. Cette technique
permet de travailler naturellement et simultanément en développement
dirigé par les tests et développement dirigé par la documentation.
L'ensemble de ces pratiques apportent des avantages indéniables :

  - garantie de non-régression
  - documentation existante et à jour
  - détection précoce des erreurs de conception
  - possibilités d'intégration continue

En Python il est également possible et conseillé d'intégrer une (petite) partie
de la documentation directement dans le code source sous forme de
docstring, qui peuvent elles-même être des doctests ! On peut donc placer
des petits tests documentés directement dans le corps du programme, à
l'intention des développeurs. Cette documentation intégrée dans le code peut
ensuite être testée et extraite automatiquement afin de générer
automatiquement des livres de documentation et des rapports de tests. Dans
ce cas d'utilisation, la proximité entre le code et les tests garantissent que les
tests sont adéquats et modifiés en même temps.

Parmi l'éventail des outils de tests, on trouve également des outils de mesure
du taux de couverture des tests, qui sont souvent intégrés par défaut dans
les outils de lancement de test et doivent faire partie de l'indicateur final de
qualité.

Les outils de Mock sont également utiles et aident à simuler un composant
applicatif manquant pour les tests.
