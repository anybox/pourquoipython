---
tags:
    - Forge logicielle
---

# Trac

![Trac](/img/trac.png)

Les forges sont des environnements conviviaux regroupant dans une interface
unifiée tous les outils utiles pendant les phases de développement d'un
logiciel : gestion des tickets, wiki de documentation, navigation dans le code,
gestion des traductions, affichage des métriques de qualité, affichage du
résultat des tests automatisés, etc. Ces forges peuvent être installées en
interne dans les entreprises et peuvent gérer souvent plusieurs projets en même
temps. Trac en fait partie : cette forge s'installe en quelques minutes et
bénéficie de nombreux modules offrant des fonctionnalités additionnelles.
Certaines de ces forges possèdent un pendant public comme le Launchpad de
Canonical qui gère de très nombreux projets de logiciels libres, notamment le
système d'exploitation Ubuntu Linux. Le Launchpad a été libéré depuis quelques
mois, il offre un niveau fonctionnel très large pour les entreprises souhaitant
s'en équiper.
