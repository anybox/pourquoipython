# GTK

![GTK](/img/gtk.png)

GTK+ utilise automatiquement le thème natif de Windows et
passe donc inaperçu sur ce système. Il est utilisé par défaut
dans beaucoup de systèmes d'exploitations comme Ubuntu,
Redhat ou Solaris, car l'environnement Gnome se base
entièrement sur lui. De très nombreux logiciels l'utilisent, aussi bien libres,
comme GIMP (retouche photo), Inkscape (illustrateur vectoriel) ou Maemo
(plateforme embarquée), que propriétaires, comme VMware (virtualisation).
L'accès en Python se fait grâce à PyGtk, disponible sous Windows, Linux et
MacOsX.
