# Interfaces graphiques

Python est fourni par défaut avec un module « TkInter » permettant de créer
des interfaces graphiques simples. Pour les besoins plus avancés, il peut
s'interfacer avec plusieurs bibliothèques graphiques présentées plus bas. Le
code créé peut en outre fonctionner de la même manière sur Windows, Linux,
MacOsX ou n'importe quel autre système sur lequel la bibliothèque utilisée est
disponible ! Visuellement il est impossible de différencier une application
écrite en Python et la même application écrite par exemple en C ou C++ : si
la bibliothèque graphique est la même, le rendu est identique. Le code source,
par contre, sera beaucoup plus facile à comprendre et à maintenir.
L'utilisation de Python avec ces bibliothèques réduit les temps de
développement de manière spectaculaire, sans pénaliser la réactivité des
logiciels.

## Bibliothèques disponibles en Python

Une bibliothèque graphique est le composant qui offre l'ensemble des widgets
composant une interface graphique (boutons, checkbox, menus, ascenseurs,
etc.). Plusieurs bibliothèques existent pour créer des interfaces graphiques
multiplateformes, et toutes peuvent être utilisées avec Python. Les plus
connues sont GTK+, Qt et WxWidgets.

D'autres bibliothèques similaires existent, comme Tk, dont l'interface est
même fournie par défaut avec Python, ou Kivy pour des interfaces multitouch.
