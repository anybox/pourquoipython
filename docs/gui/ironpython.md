# IronPython

IronPython est une version de Python fonctionnant sur la plateforme .NET. Il
permet d'accéder à toutes les bibliothèques .NET et donc de créer des
interfaces graphiques avec WPF, MFC ou directement win32.
