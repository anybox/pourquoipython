# Jython

Jython est une version de Python fonctionnant sur la plateforme Java. Il
permet d'accéder en Python à toutes les bibliothèques Java et donc de créer
des interfaces graphiques avec AWT ou Swing.

