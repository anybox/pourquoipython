# Kivy

![Kivy](/img/kivy.png)

Kivy est un framework de développement rapide d'applications faisant usage
d'interfaces innovantes comme les interfaces tactiles multipoints, et
fonctionne sous Linux, MacOsX, Windows, Android et iOS.

![Kivy](/img/kivy-exemple.png)
