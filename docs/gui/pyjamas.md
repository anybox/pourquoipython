# Pyjamas

Pyjamas provient des technologies web. Il s'agit d'un compilateur Python
vers Javascript, d'un framework AJAX et d'un ensemble de widgets pour créer
une application fonctionnant indifféremment sur le web ou en mode desktop.
