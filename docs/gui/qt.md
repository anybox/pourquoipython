# Qt

![QT](/img/qt.png)

Qt est une bibliothèque développée aujourd'hui par Nokia et aussi
très employée, aussi bien par des logiciels libres que des logiciels
propriétaires. Elle est utilisée également par de nombreux
systèmes d'exploitation comme Mandriva ou Pardus, car
l'environnement KDE se base dessus. L'accès en Python se fait grâce à PyQt,
disponible sous Windows, Linux et MacOsX.
