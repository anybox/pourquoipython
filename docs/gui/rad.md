# Création rapide d'interfaces graphiques

Pour créer rapidement des interfaces graphiques complexes, il existe des
outils interactifs et très simples à utiliser. En voici quelques uns, dont certains
sont écrits en Python. Leur fonctionnement est similaire : vous créez
visuellement l'interface graphique, puis l'outil enregistre le description de
cette interface dans un fichier XML. Ce fichier XML peut ensuite être utilisé
grâce à une unique instruction dans le programme final en Python. Avec
WxWidgets, étant donné que le code fait plus d'une ligne, il est généré
automatiquement par l'outil wxGlade.

![GTK RAD](/img/gtk-rad.png)

![QT RAD](/img/qt-rad.png)

![WX RAD](/img/wx-rad.png)

## Une alternative à Glade

Il existe un programme équivalent à Glade, appelé Gazpacho et entièrement écrit
en Python. Son interface est légèrement différente, mais il offre globalement
les mêmes fonctionnalités, plus l'accès à un 3.0 ensemble supplémentaire de
widgets (Kiwi).

