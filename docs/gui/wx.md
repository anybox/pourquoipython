# WX Widgets

![WX Widgets](/img/wx.png)

Le principal intérêt de WxWidgets est qu'il utilise la
bibliothèque graphique du système cible, rendant
l'intégration 100% transparente. L'accès en Python se
fait grâce à WxPython, disponible sous Windows, Linux et MacOsX.
