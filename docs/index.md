---
hide:
  - toc
---
# Pourquoi Python ?

Voici un tour d'horizon de la plateforme

Python est un langage et une plateforme de développement logiciel complète et généraliste, très facile d'accès et capable de se spécialiser de manière très pointue dans la majorité des domaines informatiques. 

Python est utilisé par un public très large : des développeurs web professionnels, des chercheurs en intelligence artificielle ou en bio-informatique, des administrateurs systèmes, ou même des programmeurs occasionnels. C'est le mélange de polyvalence et de facilité qui fait la force de Python. Avec un bref apprentissage et un minimum d'efforts, vous serez capable d'envisager n'importe quel type d'application de manière extrêmement efficace et de la terminer (ou de la faire terminer) en temps voulu.

Le développement de Python ayant commencé à la fin des années 1980, son déploiement dans l'industrie a commencé vers les années 2000. Aujourd'hui, Python est devenu très populaire auprès des développeurs, des scientifiques et d'un nombre croissant de profils qui ont des besoins d'automatisation. Beaucoup de projets viennent peupler un écosystème déjà très riche, et ce dans tous les domaines. La plateforme bénéficie donc d'une visibilité croissante, qui s'accentuera encore dans les prochaines années.

## À quoi ressemble la plateforme Python ?

* Un langage dynamique, interactif, interopérable et très lisible
* Un vaste ensemble de bibliothèques et cadriciels spécialisés
* Des outils d'édition, de test et d'industrialisation
* Le support d'une communauté d'entreprises, d'individus et d'associations
* Un marché en forte croissance

TODO : donner ici plusieurs axes de navigation
