# Jeux Vidéos

Le domaine des jeux vidéos n'est pas non plus ignoré : Dans le jeu en ligne
massivement multijoueurs **EVE online**, Stackless Python a été utilisé pour
implémenter le serveur de jeu, un système demandant une approche hautement
concurrentielle de la programmation et de grandes performances. La souplesse de
Python permet ici de créer un design applicatif extensible, adapté à un besoin
en évolution constante, tout en restant facile à modifier et améliorer dans des
délais brefs. Dans le jeu **Civilisation IV**, un jeu de stratégie réinventant
l'histoire des civilisations humaines sur plusieurs milliers d'années, Python
est utilisé pour accéder à de nombreux composants du jeux, et scripter un
certain nombre de fonctionnements.  Il existe également des moteurs 3D comme
les moteurs **Panda3D**, ou **Soya3d**, dont les cœurs sont respectivement en C++ et
Python/Pyrex et avec lesquels on peut écrire des jeux entièrement en Python et
avec de bonnes performances.

![Jeu vidéo](/img/jeu-video.png)
