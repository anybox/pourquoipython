# Apprentissage

La simplicité d'utilisation de Python en fait un langage intéressant pour
apprendre l'algorithmique sans être perturbé par des considérations telles que
la compilation, la gestion des pointeurs et de la mémoire ou la maîtrise
d'outils liés au langage. Juste après installation, on peut immédiatement
lancer un interprète et effectuer quelques tests en ligne de commande. À ce
stade, même un éditeur de code est inutile.
