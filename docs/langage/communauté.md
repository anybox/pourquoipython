# Communauté

Le langage Python est développé sur le mode communautaire et n'est pas
dépendant d'une seule entreprise. Il est le résultat de travaux et de votes
issus de propositions d'améliorations nommées « PEP » (Python Enhancement
Proposal). Au delà du langage en lui-même, la création et le support de
l'ensemble des outils et bibliothèques de l'écosystème est pris en charge par
une communauté internationale constituée d'entreprises, particuliers,
organisations ou associations.
En France, le côté associatif est
animé par l'afpy, au travers de
nombreuses activités :

  - conférence annuelle PyConFR
  - rencontres mensuelles sur des sujets techniques et non techniques
  - présence aux salons, rencontres et conférences :
  - Salon Solutions Linux
  - RMLL
  - OSDC fr
  - JDLL
  - JM2L
  - Rédaction de dossiers et articles de presse(Hors-Série Linux Magazine n°40 sur Python)
  - Animation du site web [afpy.org](https://afpy.org) (forum, liste de diffusion, offres d'emploi, etc.)
  - Présence sur des canaux IRC

![AFPY](/img/afpy.png)
