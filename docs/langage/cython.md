# Cython

Cython produit des modules C à partir du code python, permet de rendre les
types de variables statiques, offrant dans ce cas de grandes optimisations.
C'est l'outil parfait pour accélérer les zones d'un programme qui doivent
absolument être rapides.

