# Environnements de développement intégrés

Les développeurs habitués aux environnement de développement intégrés pourront
retrouver leurs marques car il est possible de travailler notamment avec
Eclipse en utilisant un module nommé PyDev, qui offre tous les outils
classiques d'un EDI avec une spécialisation pour Python : complétion de code,
colorisation de syntaxe, analyse et évaluation de code, etc.  D'autres EDI non
mentionnés ici sont disponibles, aussi bien libres que propriétaires et souvent
écrits eux-mêmes en Python.

![Eclipse](/img/eclipse.png)
