# À quoi ressemble Python ?

## Un langage facile et lisible

Python se caractérise par une syntaxe claire et lisible. Ce principe découle d'une constatation simple : un programme est lu bien plus souvent qu'il n'est écrit. Il faut donc que la compréhension du code à la lecture soit la plus rapide possible. Ceci prend même une importance considérable en milieu professionnel où un programme peut changer de mains plusieurs fois et doit être maintenu sur le long terme. 

Cette lisibilité provient de plusieurs caractéristiques :
* Un très faible nombre de caractères exotiques : un programme Python est dépourvu de la plupart des caractères incompréhensibles que l'on rencontre traditionnellement dans les langages de programmation. Par exemple pour affecter le nombre 4 à la variable a, on n'écrit pas `$a := 4;` mais simplement `a = 4`
* Une indentation obligatoire : ce sont les espaces en début de lignes qui délimitent les blocs. Cette particularité, si elle effraie certains développeurs, se révèle en fin de compte être un atout énorme : un code Python est toujours indenté correctement et se lit toujours de la même manière. L'indentation a pour le programmeur la même utilité que les accolades pour le compilateur. En limitant la redondance, Python évite ainsi le risque d'incohérence entre ce qui est dit au compilateur et ce qui est dit au programmeur.

![Syntaxe C vs Python](/img/c-vs-python.png)

Pour ces raisons, le Python se rapproche parfois des pseudo-langages utilisés pour enseigner l'algorithmique. Il est idéal pour apprendre à programmer ou pour créer rapidement un prototype d'application. À l'autre extrémité, cette lisibilité devient un énorme avantage devant la complexité des très grands logiciels. En pratique, un ensemble de conventions et de consignes simples a été proposé pour encadrer l'écriture de code Python. Ces règles sont définies dans un document portant le nom de « PEP 8 », qui tend à homogénéiser les programmes Python et à favoriser l'échange et la collaboration.

## Un mode interactif et un débogueur intégré

Python se caractérise par une syntaxe claire et lisible. Ce principe découle d'une constatation simple : un programme est lu bien plus souvent qu'il n'est écrit. Il faut donc que la compréhension du code à la lecture soit la plus rapide possible. Ceci prend même une importance considérable en milieu professionnel où un programme peut changer de mains plusieurs fois et doit être maintenu sur le long terme. 

Cette lisibilité provient de plusieurs caractéristiques :
* Un très faible nombre de caractères exotiques : un programme Python est dépourvu de la plupart des caractères incompréhensibles que l'on rencontre traditionnellement dans les langages de programmation. Par exemple pour affecter le nombre 4 à la variable a, on n'écrit pas `$a := 4;` mais simplement `a = 4`
* Une indentation obligatoire : ce sont les espaces en début de lignes qui délimitent les blocs. Cette particularité, si elle effraye certains développeurs peu soigneux, se révèle en fin de compte être un atout énorme : un code Python est toujours indenté correctement et se lit toujours de la même manière. L'indentation a pour le programmeur la même utilité que les accolades pour le compilateur. En limitant la redondance, Python évite ainsi le risque d'incohérence entre ce qui est dit au compilateur et ce qui est dit au programmeur.

Pour ces raisons, le Python se rapproche parfois des pseudo-langages utilisés pour enseigner l'algorithmique. Il est idéal pour apprendre à programmer ou pour créer rapidement un prototype d'application. À l'autre extrémité, cette lisibilité devient un énorme avantage devant la complexité des très grands logiciels. En pratique, un ensemble de conventions et de consignes simples a été proposé pour encadrer l'écriture de code Python. Ces règles sont définies dans un document portant le nom de « PEP 8 », qui tend à homogénéiser les programmes Python et à favoriser l'échange et la collaboration.

## Un mode interactif et un débogueur intégré

Une autre particularité de Python est la présence d'un mode interactif : si on démarre Python sans lui donner de programme à exécuter, il donne la main à l'utilisateur, et exécute à la demande toute commande Python valide. Ceci est un atout pour la rapidité de développement, notamment lors d'un prototypage. De plus, dans ce mode il est possible de consulter l'aide des classes et des fonctions. Il est ainsi facile de faire des essais en mode interactif, puis de les recopier dans le corps du programme. Dans ce mode, Python peut aussi être utilisé comme une calculatrice programmable. 

Dans l'illustration ci-dessous, l'interprète Python standard est celui du milieu. Les deux autres, ipython et bpython sont des variantes offrant des fonctionnalités additionnelles, comme la coloration, la complétion, les suggestions pendant la frappe, etc. 

![Interprètes Python](/img/interpreteurs-python.png)

Nativement, le langage offre également un débogueur. N'importe quel programme peut ainsi être interrompu et exécuté instruction par instruction grâce à la pose de points d'arrêts ou à l'affichage de la pile d'appel. L'analyse et la correction de problèmes sont ainsi grandement facilitées : le débogueur prend l'apparence d'une console Python donnant accès à l'environnement d'exécution de l'application.

## Multi-plateforme et interopérable

Python fonctionne sur toutes les plateformes les plus courantes, Windows, Linux et Mac Os, ainsi que sur de nombreux autres systèmes, y compris des plateformes mobiles telles que Android.

![Plateformes Python](/img/plateformes-python.png)

Python fonctionne également sur la machine virtuelle Java ou sur la plateforme .NET de Microsoft, donnant ainsi un accès direct à toutes les API Java ou .NET. Les versions correspondantes s'appellent Jython et IronPython.

Enfin, le module ctypes permet d'utiliser nativement n'importe quelle bibliothèque écrites en C. Python est dans ce cas idéal comme langage de glue.

## Ouvert et libre

Les différentes versions de Python sont toutes publiées sous une licence libre permissive, autorisant l'écriture de logiciels libres ou propriétaires. Python peut être librement modifié, redistribué et même intégré à l'intérieur d'un autre logiciel pour lui offrir des capacités de scripting.

## Moderne et Multi-paradigme

Au-delà d'une syntaxe très lisible, Python possède un typage fort mais dynamique, une compilation automatique en bytecode, un garbage collector, une gestion des exceptions, de l'Unicode, de la programmation multithread et multiprocess ainsi que d'autres caractéristiques qui en font un langage moderne et de plus en plus utilisé. 

Python est un langage multiparadigme : il est possible de programmer en mode **impératif**, sans être un expert de la programmation objet. Si, en revanche, on programme avec des **objets**, on peut plonger sans difficulté dans tous les motifs de conception (Design Patterns), y compris en utilisant des interfaces ou des classes abstraites. La programmation **fonctionnelle**, enfin, peut être abordée grâce à l'importance accordée aux listes, aux itérateurs, aux générateurs ou grâce à la présence des fonctions `map`, `reduce`, `filter` et des fonctions anonymes `lambda`.
