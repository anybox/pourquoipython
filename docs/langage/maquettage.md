# Maquettage

Réaliser un algorithme intensif final en pur Python n'est pas une bonne idée,
mais faire une maquette de cet algorithme en Python en est une excellente.
Cela permet de confirmer ou infirmer une hypothèse très rapidement et
d'éviter d'allouer trop de temps ou de ressource à quelque chose qui pourra
éventuellement être abandonné.
Une fois la maquette réalisée, il suffit de réécrire uniquement les parties qui
doivent être accelérées dans un langage compilé puis de les placer dans un
module Python, ou bien d'utiliser une des techniques ci-dessous qui ont
l'avantage de rester en Python.
