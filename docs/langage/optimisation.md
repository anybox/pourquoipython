# Optimisation

Python étant un langage interprété, il profite pleinement du fait que le temps
d'exécution d'un programme coûte beaucoup moins cher que son temps de
développement : le but est de créer des programmes le plus rapidement possible,
tout en restant extrêmement lisible et maintenable.

![Benchmark](/img/benchmark.png)

Néanmoins des besoins d'optimisation peuvent survenir. Pour ce qui est des
performances algorithmiques, Python permet d'exprimer simplement des
algorithmes complexes et de comparer les performances de différentes
approches, mais ne sera pas nécessairement la plateforme de choix pour faire
tourner, in fine, lesdits algorithmes.
Diverses approches ont été explorées pour offrir des accélérations, sous la
forme d'implémentations alternatives, de compilation just-in-time, de
traduction de code ou simplement d'optimisations.
Il existe différentes techniques pouvant être utilisées, dont certaines sont
capables d'atteindre les performances du langage C.

Voici un bref graphique comparatif de quelques
benchmarks, afin de visualiser les différences potentielles entre les solutions
mentionnées ci-dessus. L'unité est le temps mis par CPython sur chaque
algorithme.

![Benchmark](/img/optimisation.png)

On constate que les possibilités d'accélération d'un code sont énormes avec
Psyco qui est probablement l'une des solutions les plus efficaces
actuellement. Stackless Python trouve tout son intérêt lors d'écriture
d'applications multithread.
