# Psyco

C'est un mécanisme de compilation just-in-time. Psyco annonce un
gain moyen de 4x, mais pouvant aller jusqu'a 100x, et ne demande aucune
modification du code source. Il ne marche cependant que sur architecture
i386.
