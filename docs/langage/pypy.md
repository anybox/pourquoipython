# PyPy

Très ambitieux et dopé un temps par un financement européen, PyPy
est un projet de recherche visant à effectuer de la traduction de code et de la
compilation. L'idée directrice est de traduire une description de Python
effectuée en Python lui-même vers des langages de plus bas-niveau. La
rumeur veut que le but recherché est d'aller plus vite que le C. PyPy
commence à connaître des retombées intéressantes et affiche des
performances souvent meilleures que le Python de référence.
