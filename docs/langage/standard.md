# Services fournis en standard

Un langage de haut niveau comme Python se caractérise par le fait qu'il évite
de réinventer ce qui existe déjà. Tout est déjà prévu pour que vous puissiez
profiter de manière simple de protocoles courant, comme HTTP, FTP, IMAP ou
POP, de techniques de programmation avancées comme le multithreading ou
le multiprocessing, ou bien de chiffrement, compression ou stockage des
données.

Voici un aperçu des possibilités fournies en standard avec Python.
Tous les types et services de base sont directement intégrés, comme les
chaînes de caractères unicode, les types numériques, la gestion des erreurs
(exceptions), la gestion des fichiers et des entrées/sorties, le formatage des
chaînes et des dates. Des structures de haut niveau sont également
présentes, comme les listes, les ensembles, les tableaux ou les dictionnaires.
Ces structures sont fortement optimisées et leur implémentation est écrite en
C. Elles apportent un haut niveau d'homogénéité et de souplesse dans la
gestion des données.

De nombreux modules spécialisés sont présents par défaut, en voici quelques
exemples :

| Nom du module     | Usage                                             |
|-------------------|---------------------------------------------------|
| re                | expressions rationnelles                          |
| difflib           | calculs de delta de différences                   |
| datetime          | opérations sur les dates et heures                |
| calendar          | opérations sur des calendriers                    |
| math              | opérations mathématiques                          |
| random            | opérations aléatoires                             |
| zlib              | opérations de compression                         |
| csv               | lecture/écriture de fichiers .CSV                 |
| ConfigParser      | lecture/écriture de fichiers .INI                 |
| sqlite3           | accès à des bases de données SQLite               |
| md5, sha          | opérations de hachage                             |
| shutil            | opérations de haut niveau sur les fichiers        |
| io                | opérations de bas niveau sur des flux de données  |
| threading         | programmation multithread                         |
| subprocess        | création de sous-processus                        |
| multiprocessing   | programmation multiprocessus                      |

Quelques exemples de modules standards pour la programmation réseau et
internet :

| Nom du module | Usage                                     |
|---------------|-------------------------------------------|
| ssl           | connexion réseau sécurisée                |
| email         | opérations sur des courriels              |
| json          | encodeur et décodeur JSON                 |
| webbrowser    | gestion des navigateurs internet          |
| cgi           | outils pour la programmation web en CGI   |
| http          | programmation HTTP                        |
| ftplib        | programmation FTP                         |
| poplib        | réception d'e-mail par POP                |
| imaplib       | gestion d'e-mail par IMAP                 |
| smtplib       | envoi d'e-mail (SMTP)                     |
| xmlrpc        | connexion à des services web XML-RPC      |

Exemples de modules utiles au développement, débogage, profilage,
également fournis en standard :

| Nom du module         | Usage                                     |
|-----------------------|-------------------------------------------|
| pydoc                 | générateur automatique de documentation   |
| doctest               | écriture de la documentation testable     |
| unittest              | écriture des tests unitaires              |
| pdb                   | le débogueur Python                       |
| cprofile et profile   | les profileurs Python                     |
| gc                    | accès au Garbage Collector                |
| inspect               | outils d'introspection des objets Python  |

D'autres modules sont spécifiques aux plateformes UNIX, Windows ou Mac Os.
Les modules ci-dessus ne sont que quelques exemples, vous pouvez vous
référez à la [documentation officielle](https://docs.python.org/) pour avoir une vision plus complète des
possibilités de la bibliothèque standard.
