# Synthèse

Les 20 ans d'âge de la plateforme Python lui ont apporté une forte maturité et
ont engendré un écosystème très varié. Cet écosystème est composé de
nombreux acteurs, institutions, indépendants, particuliers, associations, mais
surtout de très nombreuses entreprises des plus petites aux plus grandes
comme Google ou Microsoft qui ont compris les avantages - agilité,
polyvalence, lisibilité et efficacité - et l'engouement que pouvait apporter ce
langage. Dans de nombreux pays et secteurs, on constate même une forte
accélération du taux d'utilisation de Python. Tournée vers l'avenir, la
plateforme a également réussi sa modernisation avec la sortie d'une nouvelle
version majeure (3.0 à 3.4) qui se débarrasse de certains défauts de jeunesse.
Or, en dépit d'une bonne industrialisation de ses procédés et la présence de
nombreux outils, Python reste une plateforme extrêmement facile à aborder
et très homogène. Cette homogénéité tire sa source d'une sensibilité propre à
la communauté, qui se retrouve dans l'écriture de ce qu'on appelle un code
« pythonique ». Qu'est-ce qu'un code pythonique ? Une première réponse
existe dans l'interprète Python lui-même :

```python
>>> import this
The Zen of Python, by Tim Peters
Beautiful is better than ugly.
Explicit is better than implicit.
Simple is better than complex.
Complex is better than complicated.
Flat is better than nested.
Sparse is better than dense.
Readability counts.
Special cases aren't special enough to break the rules.
Although practicality beats purity.
Errors should never pass silently.
Unless explicitly silenced.
In the face of ambiguity, refuse the temptation to guess.
There should be one -- and preferably only one – obvious way to do it.
Although that way may not be obvious at first unless you're Dutch.
Now is better than never.
Although never is often better than *right* now.
If the implementation is hard to explain, it's a bad idea.
If the implementation is easy to explain, it may be a good idea.
Namespaces are one honking great idea -- let's do more of those!
```
