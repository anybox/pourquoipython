# Biopython

C'est un framework orienté biologie. Il contient
plusieurs modules spécialisés, pour travailler
avec les séquences ADN, les protéines qu'elles
codent et interagir avec les principales bases
de données biologiques en ligne.

![Biopython](/img/biopython.png)
