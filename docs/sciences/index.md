# Python pour les sciences

Le domaine scientifique est un des plus gros points forts de Python. Sa facilité d'apprentissage permet à des scientifiques, chercheurs, mathématiciens d'être efficaces rapidement lors de prototypages, de calculs lourds ou distribués, de visualisation, d'apprentissage automatique. Python remplace progressivement les outils ou frameworks scientifiques propriétaires et son modèle Open Source est un avantage dans le milieu scientifique où le partage de connaissance et d'innovation est une pratique naturelle.

![Python pour les Sciences](/img/python-science.png)

Une liste en anglais se trouve à l'adresse suivante :

[https://wiki.python.org/moin/NumericAndScientific](https://wiki.python.org/moin/NumericAndScientific)
