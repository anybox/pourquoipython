# Matplotlib

![Matplotlib](/img/matplotlib.png)

Matplotlib est une bibliothèque de tracé et visualisation produisant des
graphiques de qualité professionnelle. Les graphiques peuvent être produits
dans différents formats de sortie, y compris des formats interactifs permettant
une interaction à la souris, intégrables dans une interface graphique.
Matplotlib peut être utilisé soit depuis un programme en Python, soit depuis un
terminal de commande interactif. De très nombreux types de graphiques peuvent
être générés. Par défaut il s'agit de graphiques en 2D et des extensions
permettent de produire des cartographies, graphiques en 3D ou des grilles de
données.

![Exemples Matplotlib](/img/matplotlib-exemples.png)
