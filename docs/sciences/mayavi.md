# MAYAVI

Outil de visualisation interactive de données scientifiques,
prévu pour être intégré avec les différentes bibliothèques
scientifiques Python (notamment Scipy).

![Mayavi](/img/mayavi.png)
