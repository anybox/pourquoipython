# Molecular Modeling Toolkit

MMTK permet la modélisation et la
manipulation de molécules, grâce à des algorithmes de simulations
permettant d'implémenter facilement des simulations complexes (trajectoires,
codage de protéines étape par étape...).
