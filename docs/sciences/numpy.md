# NumPy

[NumPy](https://numpy.org/) est l'outil de base pour faire du calcul scientifique en Python. Il offre notamment des capacités de calcul sur des matrices à _N_ dimensions, des capacités d'intégration avec C/C++ et Fortran, de l'algèbre linéaire, des transformées de Fourier et des outils de calcul aléatoire. NumPy permet de travailler sur des très gros jeux de données en minimisant la consommation mémoire (calcul sur des sous-matrices, sans duplication). Les algorithmes de calcul sont implémentés en C et fortement optimisés.

![Formules](/img/formules.png)
