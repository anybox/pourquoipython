# PyMol

![PyMol](/img/pymol.png)

PyMol est un système de visualisation moléculaire, un outil de rendu et un
éditeur moléculaire en 3D, dédié à la visualisation de structures chimiques, y
compris les structures cristallines à résolution atomique. PyMol peut être
utilisé pour visualiser des protéines, des acides nucléiques (ADN, ARN) des
glucides, ou n'importe quelle structure moléculaire ou atomique pour la
recherche pharmacologique, biotechnologique, dans l'industrie, la recherche
académique ou l'enseignement. PyMol permet de générer des images statiques ou
des animations et peut exporter les données vers d'autres formats 3D.

![PyMol](/img/pymol-exemple.png)

