# Sage

![Sage](/img/sage.png)

L'objectif de Sage est l'étude des mathématiques, élémentaires ou avancées,
fondamentales ou appliquées. Cela comprend notamment l'algèbre basique, le
calcul infinitésimal, la théorie des nombres, la cryptographie, le calcul
numérique, l'algèbre commutative, la théorie des groupes, la combinatoire, la
théorie des graphes, l'algèbre linéaire exacte et beaucoup d'autres domaines.
Sage est dédié à l'enseignement et la recherche. Son principe est de rassembler
plus d'une centaine de programmes Open Source dans une interface unifiée, soit
une ligne de commande Python, soit un « notebook » accessible depuis un simple
navigateur web.

![Sage](/img/sage-exemple.png)
