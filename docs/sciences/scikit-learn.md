# Scikit Learn

![Scikit-Learn](/img/scikit-learn.png)

Construit à partir des trois outils précédents, Scikit Learn est un ensemble de
bibliothèques et d'algorithmes dédiés à l'apprentissage automatique, au data
mining et à l'analyse de données, c'est à dire aux problèmes de classification,
régression, clustering, réduction dimensionnelle, sélection de modèle,
extraction de caractéristiques et normalisation.

![Exemple Scikit-Learn](/img/scikit-learn-exemples.png)
