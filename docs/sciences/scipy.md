# SciPy

SciPy est construit au dessus de NumPy et offre un vaste
ensemble d'outils et d'algorithmes pour les mathématiques,
le calcul scientifique et l'ingénierie : calculs matriciels,
polynomiaux, algèbre linéaire, traitement du signal,
statistiques, algorithmes génétiques, machine learning, etc.

![SciPy](/img/scipy.png)
