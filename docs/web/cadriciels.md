# Cadriciels de développement web

L'intérêt d'un framework (ou « cadriciel » en français) est d'offrir de manière
cohérente toutes les briques de base pour créer une application. Ainsi, les
frameworks se chargent de vous offrir toutes les facilités indispensables :
accès aux bases de données, génération de page HTML, génération de
formulaires, sessions, cache, authentification, etc.
Le monde du développement web en Python est extrêmement riche. Il est
possible d'attaquer tous les niveaux de complexité, depuis la simple page
personnelle jusqu'à un gros ERP ou un intranet. Cette richesse est
heureusement contrôlée par une spécification qui rend les composants web et
les frameworks interopérables. (La spécification WSGI, Web Server Gateway
Interface).
La partie émergée de cet énorme iceberg laisse apparaître quelques
frameworks majeurs : Django, Odoo ou encore Zope. La partie immergée, tout
autant digne d'intérêt, est abordée plus bas.

## Django

![Django](/img/django.png)

Django est un framework web généraliste de type MVC, très similaire à Ruby on
Rails ou Symfony. Sa documentation est complète et sa prise en main rapide. La
communauté française est très active et anime le site web django-fr.org.
Django est idéal pour créer rapidement des sites ou applications web avec de
bonnes performances. Il peut s'interfacer avec différents types de bases de
données et prend nativement en charge MySQL, PostgreSQL, SQLite et Oracle. De
très nombreux modules pour Django existent (qu'on appelle des « applications
Django ») et peuvent se greffer pour offrir des fonctionnalités additionnelles.
Outre son excellente documentation, un des intérêts historiques de Django est
d'offrir une interface d'administration, créée automatiquement, pour votre
application.

## Odoo

![Odoo](/img/odoo.png)

Odoo, si l'on fait abstraction de tous ses modules applicatifs, est aussi un
framework de développement rapide. Il se base exclusivement sur la base de
données PostgreSQL, gage de performance et de fiabilité. Outre sa modularité,
son plus grand atout est d'être un framework beaucoup plus cadré que les deux
précédents, faisant de lui le framework idéal pour le développement d'outils de
gestion d'entreprise : l'interface graphique, la base de données, les workflows
métiers étant définis de manière quasi-descriptive, on arrive très rapidement à
obtenir un résultat attrayant et ergonomique, et on peut passer ainsi plus de
temps sur les besoins métiers.

## Zope

![Zope](/img/zope.png)


Zope est un écosystème très vaste et très
riche, certainement le plus ancien. Le principe
fondateur de Zope est la publication d'objets
sur le web : dès la fin des années 90, ce
framework était entièrement orienté objet, y
compris sa base de données (ZODB). Zope est
toujours très utilisé et a donné naissance à tout un écosystème, composé de
frameworks, micro-frameworks modernes, bibliothèques, outils et applications
comme Plone. Zope est le framework idéal pour créer des application de
gestion de contenu ou gestion documentaire.

## Autres frameworks

Les succès de Zope, Django et Odoo proviennent du fait qu'ils offrent, à
l'instar de Python, un maximum de possibilités de manière native. Ces deux
produits ne doivent pas occulter l'extraordinaire richesse d'autres frameworks
comme Pyramid, CubicWeb, web2py, CherryPy, Werkzeug, Flask, Anyblok, etc. Vous
pouvez obtenir une liste plus complète sur la page
http://wiki.python.org/moin/WebFrameworks

![Pyramid](/img/pyramid.png)

## Micro-frameworks et assemblage de composants
web
Plutôt que d'utiliser des frameworks tout-en-un comme Zope, Django, ou
Odoo, une alternative intéressante est de créer des applications par
assemblage de composants génériques. Le principe est de partir d'une base
minimale (comme Pyramid ou Flask), puis d'ajouter des briques. Cet
assemblage peut se faire de différentes façons :

  - par ajout et utilisation de bibliothèques additionnelles,
  - grâce à WSGI, un standard Python permettant de brancher des
middlewares
  - grâce à la Component Architecture de Zope, utilisable avec n'importe
quel projet Python
  - avec d'autres techniques de composants existantes (voir CubicWeb)

Voici quelques exemples de fonctionnalités pouvant être prises en charge par
des composants séparés et réutilisables :

### Accès aux bases de données

![SQLAlchemy](/img/sqla.png)

SQLAlchemy est une bibliothèque d'abstraction permettant d'accéder à n'importe
quelle base de données relationnelle de manière identique et en programmation
objet. SQLAlchemy peut dialoguer avec SQLite, PostgreSQL, MySQL, Oracle,
MS-SQL, Firebird, MaxDB, MS Access, Sybase, Informix, et DB2. D'autres outils
du même genre existent : Storm, SQLObject. Nous vous invitons à consulter la
[section Bases de données](/bdd/).

### Génération de formulaires

Le principe est de décrire les données grâce à un schéma puis de laisser les
formulaires se générer automatiquement en fonction du schéma. Il existe
souvent un lien entre formulaires, schémas et bases de données. Pour cette
raison, les bibliothèques de génération de formulaires sont souvent liées à une
autre technologie, soit le framework lui-même, soit la base de données ou une
abstraction sur la base de données. Voici quelques exemples de bibliothèques
matures utilisables en production : z3c.form (Zope), FormAlchemy
(SQLAlchemy), tw.forms (TurboGears), gnue-forms (GNUe).

### Authentification / autorisation

Il est possible de prendre en charge la notion
d'authentification et d'autorisation de manière
générique, hors de tout framework, juste par
ajout d'un middleware WSGI sur une application
existante. Les composants repoze.who et
repoze.what sont conçus dans cette optique et
couvrent les deux types de besoin. Ils fonctionnent grâce à des plugins et sont
capables de gérer n'importe quelle source d'utilisateurs, de groupes, de
permissions, et n'importe quelle méthode d'authentification et d'autorisation.

![Repoze](/img/repoze.png)

### Templating

La création dynamique des pages HTML est prise en charge par ce qu'on
appelle un outil de templating. Le principe est d'insérer du contenu en plaçant
dans les pages HTML soit des balises spécifiques, soit des attributs XML. Tous
les frameworks de développement web proposent un ou plusieurs outils de ce
genre (dont les noms sont ZPT, Mako, Genshi, Chameleon, et d'autres).

### Gestion du cache et des sessions

Beaker est une bibliothèque permettant de gérer du cache ou des sessions
avec abstraction du stockage. De nombreuses méthodes de stockage sont
disponibles, persistantes ou non : memcached, dbm, sql, mémoire vive, etc.
