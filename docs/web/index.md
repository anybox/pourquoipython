# Sites et applications web

Python est utilisé depuis plus d'une décennie pour des développements web
professionnels. Certains sites web actuels à très fort traffic comme YouTube
ou Reddit ont été initialement écrits en Python. Les offres d'hébergement
mutualisé classiques à très faible coût proposent assez rarement ce langage
car il nécessite l'utilisation d'un serveur dédié ou privé virtuel. Cependant,
l'avènement des serveurs dédiés low-cost et du Cloud Computing
s'accompagne de possibilités d'héberger des applications web en Python pour
un coût modique. L'offre de Cloud Computing de Google, nommé App Engine a
d'abord été disponible exclusivement pour Python (Google est un grand
utilisateur de ce langage).

Des applications web professionnelles de type CMS (Gestion de contenu,
intranets, extranets), ERP (Gestion d'entreprise) profitent également de la
souplesse du langage et de la facilité avec laquelle on peut écrire des
programmes génériques et extensibles.

## Plone

![Plone](/img/plone.png)

Pour la gestion de contenu, les portails ou sites collaboratifs, les intranets
ou extranets, l'outil phare est Plone. Ce CMS très mature est utilisé depuis
près de quinze ans, il est complètement industrialisé, supporté par une
importante communauté internationale et possède un très grand nombre de modules
d'extension.

Pour la gestion d'entreprise, on trouve principalement trois PGI Open Source
(Progiciels de Gestion Intégrés) :

## Odoo

![Odoo](/img/odoo.png)

Odoo connaît une croissance fulgurante. Il
fonctionne entièrement en mode web et
possède un nombre impressionnant de
modules métiers. C'est à la fois un ERP
open-source très puissant, mais aussi une
plateforme web permettant de construire
très rapidement un site web, une boutique
e-commerce en ligne, un blog ou d'autres applications web directement
intégrées avec l'ERP. En outre, il se base sur un framework de développement
rapide avec lequel il est possible de développer n'importe quelle application
d'entreprise en un temps record.

## Tryton

![Tryton](/img/tryton.png)

Tryton est une plate-forme applicative de haut-niveau, d'architecture trois
tiers, sous licence GPL-3, écrite en Python et utilisant PostgreSQL comme
moteur de base de données. Le noyau de Tryton (aussi appelé cœur) est un fork
de TinyERP (aujourd'hui appelé Odoo).  Il fournit toutes les fonctionnalités
nécessaires à une plate-forme applicative complète: persistance des données,
modularité, gestion des utilisateurs (authentification, contrôle fin des accès
aux données), workflow et rapports, services web et internationalisation.
Constituant ainsi une plate-forme applicative qui peut être utilisée dans un
large éventail de situations.

## ERP5

![ERP5](/img/erp5.png)

Un autre PGI majeur est ERP5. Basé sur les technologies
Zope, celui-ci s'offre le luxe de concurrencer les meilleures
solutions d'ERP propriétaires, et son caractère générique lui
permet de s'adapter à n'importe quelle entreprise ou
organisation de n'importe quel pays. Il constitue également
la base du service Tiolive d'externalisation totale de l'infrastructure
informatique d'une entreprise.


## Comparatif

| Cadriciel      | Minimaliste           | Asyncrone*       |
| -------------- | ----------------------|------------------|
| django         | :material-close:      | :material-check: |
| flask          | :material-check:      | :material-check: |
| Fast API       | :material-check:      | :material-check: |
